#Linux Data
import numpy as np
import os 
import pandas as pd
import random
import matplotlib.pyplot as plt
users = os.listdir("users")

def path(user) : 
    return "users/"+user

def remove_items(test_list, item = "**SOF**"):
 
    # using list comprehension to perform the task
    res = [i for i in test_list if i != item]
 
    return res



def read_file(file_path) :
    file = open(file_path, 'r')
    Lines = file.readlines()
    command_sequence = []
    for line in Lines :
        command_sequence.append(line[:-1])
    
    return command_sequence

    

def map_to_ints(seq) : 

    all_commands = list(set(seq))
    dictionary = {}
    rev_dictionary = {}
    for index,command in enumerate(all_commands) :
        dictionary[command] = index
        rev_dictionary[index] = command

    mapped = pd.Series(seq).map(dictionary)
    return list(mapped),rev_dictionary


def transformer(List,dic_mapper) :
    return list(pd.Series(List).map(dic_mapper))



def data_summary(COMMAND_SEQUENCES) : 
    for user,i in enumerate(COMMAND_SEQUENCES) :
        i = pd.Series(i)
        print(f"user {user} , length is {len(i)}, number of unique commands is {i.nunique()}")
    

    temp = []
    for i in COMMAND_SEQUENCES : 
        temp = temp + i
    temp = pd.Series(temp)
    print(f"Total number of commands : {temp.nunique()}" )
    for user,i in enumerate(COMMAND_SEQUENCES) :
        i = pd.Series(i)
        print(len(i))
        temp = i.value_counts().to_dict()
        count_dict = {}
        for rank,i in enumerate(temp.keys()):
            count_dict[rank + 1] = temp[i]
        ranks = np.array(list(count_dict.keys()))
        occurances = np.array(list(count_dict.values()))
        plt.figure(figsize = (10,5))
        plt.scatter(   np.log(occurances), np.log(ranks))
        from sklearn.linear_model import LinearRegression
        plt.xlabel("log occurance")
        plt.ylabel("log rank")
        model = LinearRegression().fit(np.log(occurances).reshape(-1,1),np.log(ranks))
        plt.plot(np.log(occurances),model.predict(np.log(occurances).reshape(-1,1)))
        plt.show()
        if user == 2 :
            break
    
    
    
    
#Augmenting Linux Data
def transition_matrix(command_sequence) :
    counts = pd.Series(command_sequence).value_counts().to_dict()
    dim = len(set(command_sequence))
    Matrix = np.zeros((dim,dim))
    for e,actual in enumerate(command_sequence[1:]) :
        prev = command_sequence[e-1]
        Matrix[prev,actual] += 1
    for i in range(dim) : 
        Matrix[i] = Matrix[i] / counts[i]
    return Matrix

def generate_from_matrix(matrix,size) : 
    dim = matrix.shape[0]
    mapper = np.eye(dim)
    for i in range(dim) :
        for j in range(dim) :
            mapper[i,j] = (i<=j)
    mapper = matrix @ mapper
    
    for i in range(dim) :
        mapper[i,-1] = 1
    prev = np.random.randint(dim)
    sequence = [prev]
    for i in range(size-1) :
        temp = np.random.uniform()
        actual = np.sum(temp > mapper[prev])
        prev = actual
        sequence.append(prev)
    return sequence

def augment(sequence,number = 100, len_sample = 100) :
    sequence, rev = map_to_ints(sequence)
    new_sequence = []
    for i in range(0,len(sequence) - len_sample,len_sample) : 
        real_sequence = sequence[i:i+len_sample]
        matrix = transition_matrix(sequence)
        new_sequence = new_sequence + real_sequence + generate_from_matrix(matrix,number)
    new_sequence = list(pd.Series(new_sequence).map(rev))
    return new_sequence






COMMAND_SEQUENCES = []
for user in users : 
    COMMAND_SEQUENCES.append(read_file(path(user)))

for i in range(len(COMMAND_SEQUENCES)) :
    COMMAND_SEQUENCES[i] = remove_items(COMMAND_SEQUENCES[i])
    
temp = []
for i in COMMAND_SEQUENCES : 
    temp = temp + i
temp = pd.Series(temp)

dic_mapper = {}
counts = temp.value_counts()
for command in counts.index :
    if counts[command] > 1 :
        dic_mapper[command] = command
    else :
        dic_mapper[command] = "<UNK>"
        

for user,i in enumerate(COMMAND_SEQUENCES) :
    COMMAND_SEQUENCES[user] = transformer(i,dic_mapper)
    # COMMAND_SEQUENCES[user],_ = map_to_ints(COMMAND_SEQUENCES[user])
    # COMMAND_SEQUENCES[user] = augment(COMMAND_SEQUENCES[user])
    

