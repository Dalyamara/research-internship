from userModel import User
import pandas as pd
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
import gymnasium as gym
from gymnasium import spaces
from stable_baselines3 import A2C,PPO
from stable_baselines3.common.env_checker import check_env
from agentDev import markovianAgent
from sb3_contrib import RecurrentPPO



class UserNoMemory(gym.Env) : 
    def __init__(self,args = {}) :
        super(UserNoMemory,self).__init__()

        # self.adv = adv
        # self.ini = ini
        # self.cum = cum
        # self.inter = inter
        # self.dtyp = dtyp
        self.args = args
        self.markov = markovianAgent(16)
        

        self.adv = np.random.uniform(0,1)
            

        self.ini = 10**(np.random.uniform(-5,-1))
            
        

        self.cum = np.random.uniform(0.2,0.7)

        self.inter = np.random.uniform(0,1)
        self.past_mem = 0
        self.time = 0
        self.User = User(self.adv,self.ini,self.cum,self.inter,"d")
        self.actions = ['-1'] + [i for i in range(12)]
        self.action_space = spaces.Discrete(len(self.actions))
        # self.observation_space = spaces.Discrete(12)
        self.observation_space = spaces.MultiDiscrete([16,2])
        # self.observation_space = spaces.Box(  low = - np.inf , high = np.inf , shape = (18,),dtype = np.float64 )
        self.init_adversity = self.User.adversity

        
    def step(self,action) : 
        if action == 0 :
            action = "-1"
        else :
            action = action -1
        reward,next_command,shortcut,done = self.User.interact(action)
        observation = next_command
        self.time +=1
        observation = np.array((observation,shortcut*1))
        _ = self.markov.get_action(next_command,shortcut)
        # arr = self.markov.transition[next_command]
        # arr = arr / (arr.sum() +0.001)
        # observation = np.append(observation,arr)
        
        mem_state = np.array(list(self.User.memory_state.values()))
        temp = sum(list(self.User.memory_state.values()))
        
        # reward = (action == "-1")*shortcut*0.1 + (action != "-1")*reward
        # reward = reward
        # reward = temp 
        # reward = - self.User.adversity
        reward = shortcut*10.1
        

        # observation = np.append(observation,mem_state)

        info = shortcut
        return observation,reward,done,False,{"action":action,"command":next_command,
                                              "shortcut":shortcut,"adversity":self.User.adversity,
                                              "mem":self.User.memory_state}
    def reset(self,seed=42) :
        
        self.adv = np.random.uniform(0,1)
            

        self.ini = np.random.uniform(1e-5,0.1)
            
        

        self.cum = np.random.uniform(0.2,0.7)

        self.inter = np.random.uniform(0,1)
        
        
        
        self.User = User(self.adv,self.ini,self.cum,self.inter,"z")
        self.init_adversity = self.User.adversity
        return np.array([1,0]),{}


    def close(self):
        pass
    
    
    
    
class UserWMemory(gym.Env) : 
    def __init__(self,args = {}) :
        super(UserWMemory,self).__init__()

        # self.adv = adv
        # self.ini = ini
        # self.cum = cum
        # self.inter = inter
        # self.dtyp = dtyp
        self.args = args
        
        

        self.adv = np.random.uniform(0,1)
            

        self.ini = 10**(np.random.uniform(-5,-1))
            
        

        self.cum = np.random.uniform(0.2,0.7)

        self.inter = np.random.uniform(0,1)
        self.past_mem = 0
        
        self.User = User(self.adv,self.ini,self.cum,self.inter,"d")
        self.init_adversity = self.User.adversity
        self.actions = ['-1'] + [i for i in range(12)]
        self.action_space = spaces.Discrete(len(self.actions))
        # self.observation_space = spaces.Discrete(12)
        self.observation_space = spaces.Box(  low = - np.inf , high = np.inf , shape = (14,),dtype = np.float64 )
        
        
    def step(self,action) : 
        if action == 0 :
            action = "-1"
        else :
            action = action -1
        reward,next_command,shortcut,done = self.User.interact(action)
        observation = next_command
        observation = np.array((observation,shortcut*1))
        mem_state = np.array(list(self.User.memory_state.values()))
        temp = sum(list(self.User.memory_state.values()))
        # reward = (action == "-1")*shortcut*0.1 + (action != "-1")*reward
        # reward = reward
        # reward = temp 
        # self.past_mem = temp
        reward = shortcut*10.1
        observation = np.append(observation,mem_state)

        info = shortcut
        return observation,reward,done,False,{"action":action,"command":next_command,
                                              "shortcut":shortcut,"adversity":self.User.adversity,
                                              "mem":self.User.memory_state}
    def reset(self,seed=42) :
        
        self.adv = np.random.uniform(0,1)
            

        self.ini = np.random.uniform(1e-5,0.1)
            
        

        self.cum = np.random.uniform(0.2,0.7)

        self.inter = np.random.uniform(0,1)
        
        
        
        self.User = User(self.adv,self.ini,self.cum,self.inter,"z")
        self.init_adversity = self.User.adversity
        return np.array([1,0] + [0]*12).astype(dtype = np.float64),{}


    def close(self):
        pass
    
    

    
    
def test_model(model, args = None ) : 
    """
    args should be of this form : args={"adv":0.75,"inter":0.8,"cum":0.6,"ini":0.1}
    """
    full_shortcut = np.zeros(999)
    # Enjoy trained agent
    full_shortcuts = np.zeros(999)
    full_rewards = np.zeros(999)
    actions = []
    adv = []
    mem = []
    for i in tqdm(range(100)) :
        vec_env = model.get_env()
        obs = vec_env.reset()
        done = False
        short = []
        reward = []
        shortcuts = {}
        rec = 0
        lstm = None
        while not(done) : 
            action, lstm = model.predict(obs , state= lstm,deterministic=False)
            obs, rewards, done, info = vec_env.step(action)
            short.append(info[0]["shortcut"]*1)
            reward.append(rewards)
            rec += (action != 0)*1 
        adv.append(  info[0]["adversity"]  )
        mem.append(   np.sum(list(info[0]["mem"].values()))       )
        actions.append(rec)
        full_shortcuts += np.array(short)
        full_rewards += np.array(reward).flatten()

    actions = np.array(actions)
    summary = {"actions_avg":actions.mean(),"action_std":actions.std(),"reward_mean": (full_rewards/100).mean() ,
               "shortcut_use":(full_shortcuts)[-10:].mean(),"shortcut_std":(full_shortcuts)[-10:].std(),
                "adv":np.array(adv).mean() , "mem":np.array(mem).mean(),"mem_std":np.array(mem).std()}
    plt.figure(figsize=(7,5))
    plt.plot(full_shortcuts)
    plt.title("shortcut usage percentage")
    plt.show()

    plt.figure(figsize=(7,5))
    plt.plot(full_rewards/100)
    plt.title("Reward mean")
    plt.show()
    print(summary)
    return summary

