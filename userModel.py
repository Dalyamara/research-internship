import numpy as np
import random
from dataGen import linux,uniform,zipf
from dataGen2 import zipfDep

def sigmoid(x) :
    return 1 / (1 + np.exp(-x))




class User :
    def __init__(self,adversity, init_forget , cumul_forget , interruptibility ,data= "z") :
        self.adversity = adversity
        self.init_forget = init_forget
        self.cumul_forget = cumul_forget
        self.interruptibility = interruptibility
        
        #Sequence of commands to be given
        if data == "l" :
            self.command_sequence = linux()
        elif data == "z" :
            self.command_sequence = zipf(n_elements = 16,size = 1000, parameter_a = 0.5)
        elif data == "d" :
            self.command_sequence = zipfDep(1000,num_commands=16,zipf_param=0.5,sparsity=50)
            
        
        COMMAND_LIST = set(self.command_sequence)
    
        #recall probability for each command
        self.memory_state = {i:0 for i in COMMAND_LIST}
        
        #Number of executions for each command
        self.command_count = {i:0 for i in COMMAND_LIST}
        
        #Elapsed time since command execution 
        self.last_exec = {i:10 for i in COMMAND_LIST}
        
        #Number of shortcut executions for each command
        self.command_count_shortcut = {i:0 for i in COMMAND_LIST} 
        
        #Time elapsed since last shortcut execution
        self.last_exec_shortcut = {i:5 for i in COMMAND_LIST}
        
        #Confidence level : confidence in typing the shortcut correctly
        self.confidence_level = {i:0 for i in COMMAND_LIST}
        
        #Discrete time, t > t+1 when the user executes a command
        self.time = 0
        
        #memory effect
        self.effect = {i:0 for i in COMMAND_LIST}
        
 
    # gives the memory state at each time t 
    # We will use an exponential forgetting model
    def memory_update(self) :
        for com in self.memory_state.keys() :
            deltaT = self.last_exec_shortcut[com]
            use_num = self.command_count[com]
            use_num_shortcut = self.command_count_shortcut[com]
            if use_num_shortcut == 0 :
                prob= 0
            else : 
                prob = np.exp(-deltaT*self.init_forget*(1-self.cumul_forget)**use_num_shortcut)
            if self.effect[com] != 0 : 
                self.effect[com] = self.effect[com] - 1
                self.command_count_shortcut[com] -= 0.25
            
            self.memory_state[com] = prob
        
        
        #effect of using a command on class parameters
    def use_command(self,command,shortcut = False) :
        self.command_count[command] += 1
        self.last_exec[command] = 0
        if shortcut :
            self.command_count_shortcut[command] += 1
            self.last_exec_shortcut[command] = 0
        
        #Prepare for time t+1
        self.time += 1
        
        for i in self.command_count.keys() :
            self.last_exec[i] += 1
            self.last_exec_shortcut[i] += 1
            self.memory_update()
            
    
    def apply_notification_effect(self,recommendation) :
        t = self.time
        next_command = self.command_sequence[t+1]
        reward = 0
        relevant = False
        if recommendation in self.command_sequence[t+1: t+4] :
        #This is the condition of the relevancy of the shortcut recommended
        #We will consider it relevant if the user plans on using that command in the short term
            temp = self.command_sequence[t+1 : t+4]
            reward = 0.6 - temp.index(recommendation)/20
            relevant = True
        
        if relevant  : #How beneficial is the beneficial suggestion
            reward -= self.interruptibility*self.memory_state[recommendation]
            self.effect[recommendation] = 4
            # self.memory_state[recommendation] = self.memory_state[recommendation]*0.1 + 0.9
            self.last_exec_shortcut[recommendation] = 0
            self.command_count_shortcut[recommendation] += 1
            self.memory_update()
        else : 
            reward = -self.interruptibility
            # self.memory_state[recommendation] = 0.9
            # self.effect[recommendation] = 2
        return reward
    
    def apply_no_notification_effect(self) :
        #maybe soften adversity here
        return 0
        

   
    def take_action(self,next_command) : 
        #Returns a boolean of whether or not the user will use the shortcut

        return (np.random.uniform() < self.memory_state[next_command] )

   
    def interact(self,recommendation) :
        #if the agent chooses to not give a notification
        #then recommendation will be "-1"
        t = self.time
        adverse = (np.random.uniform() < self.adversity)
        recommendation_exist = (recommendation != "-1" ) 
        
        if not(adverse) and recommendation_exist :
            reward = self.apply_notification_effect(recommendation)
        else :
            reward = self.apply_no_notification_effect()
            
        shorctut = self.take_action(self.command_sequence[t+1])
        next_command = self.command_sequence[t+1]
        self.use_command(next_command,shorctut)
        self.adversity *= np.exp(-0.03*reward)
        self.adversity = min(self.adversity,0.98)
        self.adversity = max(self.adversity,0.03)
        done = (t == len(self.command_sequence)-2)
        return reward,next_command,shorctut,done
        