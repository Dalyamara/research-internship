#Complexity 0
#Uniform Generate
import random
import numpy as np
import pandas as pd 
from linuxData import COMMAND_SEQUENCES,map_to_ints
import random 

def uniform(n_elements,size) :
    elements = np.random.randint(0,n_elements,size=size)
    return elements

# commands = uniform(10,100000)



#Complexity 1
#Zipf Generate

def zipf(n_elements,size, parameter_a = 1) :
    #Freq * rank**a = constant
    ranges = [1]
    probabilities = np.array([1/(i+1)**parameter_a for i in range(n_elements)])
    probabilities = probabilities/probabilities.sum()
    # print(probabilities)
    elements = []
    sequence = [i for i in range(n_elements)]
    for i in range(size) : 
        elements.append(np.random.choice(sequence, p = probabilities ) )
    return elements

# commands = zipf(10,100000,1)


#Level 2
#Linux Generate

def linux() :

    elements = random.choice(COMMAND_SEQUENCES)
    start = np.random.randint(0,len(elements)-1000)
    elements = elements[start:start + 1000]
    elements,_ = map_to_ints(elements)
    return elements

# commands = zipf(10,100000,1)




def get_goal_measure(num_commands=12,zipf_param=1) :
    '''
    Returns the stationary probability of a zipf distribution that we want to eventually achieve by a markov chain
    '''
    goal = np.zeros(num_commands)
    for i in range(num_commands):
        goal[i] = 1/ (i+1)**zipf_param
    goal = goal / goal.sum()
    return goal

def initialize_matrix(num_commands) :
    '''
    initializes a transition matrix
    '''
    indexes = [(i,j) for i in range(num_commands) for j in range(num_commands)]
    P = np.zeros((num_commands,num_commands))
    irredcutible = False
    for i in range(num_commands) :
        P[i] = np.random.uniform(0.01,0.99,num_commands)
        P[i] = P[i] / P[i].sum()     
    return P

    


def revert_to_one(matrix) :
    '''
    returns any matrix as a matrix in which every line sums up to 1
    '''
    for i in range(matrix.shape[0]) :
        matrix[i] = matrix[i]/matrix[i].sum()
    return matrix
def get_trial_pi(matrix) :
    eigs = np.linalg.eig(matrix.T) 
    for index,eig_val in enumerate(eigs[0]) :
        if np.linalg.norm(eig_val - 1) < 0.001 :
            break
    trial_pi = eigs[1][:,index]
    trial_pi = trial_pi.real
    trial_pi = trial_pi / trial_pi.sum()
    return trial_pi




def verify_irreductibility(matrix) : 
    dim = matrix.shape[0]
    carrier = matrix.copy()
    for i in range(dim):
        carrier += carrier @ matrix
    if 0 in carrier : 
        return False
    return True

#Zipf with Dependencies
def get_matrix(num_commands=12,zipf_param=1) :
    '''
    gets a random transition matrix for which the stationary distribution follows a zipf
    '''
    P = initialize_matrix(num_commands)
        
    goal = get_goal_measure(num_commands,zipf_param)
    
    conv = False
    max_iter = num_commands*1000
    eps = 0.02
    i=0
    exp = 0.5
    while not(conv) : 
        trial = get_trial_pi(P)
        # to_change = np.argmax(np.abs(trial-goal))
        to_change = np.random.randint(0,num_commands)

        # choice_2 = np.random.randint(0,num_commands)
        # if np.random.uniform()< exp : 
        #     to_change = choice_2
        choice = np.random.randint(0,num_commands)
        P[choice,to_change] = P[choice,to_change] * 1.5**( (goal-trial)[to_change] )
        P = revert_to_one(P)
        i+= 1
        conv = np.linalg.norm(trial-goal) < eps
        if i>max_iter : 
            # print("max iterations reached")
            return P
    return P



def transition_matrix(command_sequence) :
    '''
    gets a transition matrix from a sequence
    '''
    counts = pd.Series(command_sequence).value_counts().to_dict()
    dim = len(set(command_sequence))
    Matrix = np.zeros((dim,dim))
    for e,actual in enumerate(command_sequence[1:]) :
        prev = command_sequence[e-1]
        Matrix[prev,actual] += 1
    for i in range(dim) : 
        Matrix[i] = Matrix[i] / counts[i]
    return Matrix



def generate_from_matrix(matrix,size) :
    
    dim = matrix.shape[0]
    mapper = np.eye(dim)
    # for i in range(dim) :
    #     for j in range(dim) :
    #         mapper[i,j] = (i<=j)
    # mapper = matrix @ mapper
    commands = [i for i in range(dim)]
    for i in range(dim) :
        mapper[i,-1] = 1
    prev = np.random.choice(commands)
    sequence = [prev]
    for i in range(size-1) :
        # temp = np.random.uniform()
        # actual = np.sum(temp > mapper[prev])
        actual = np.random.choice(commands,p = matrix[prev])
        prev = actual
        sequence.append(prev)
    return sequence

def zipfDep(size,num_commands=12,zipf_param=1,seed=None) : 
    if seed is not None :
        np.random.seed(seed)
    matrix = get_matrix(num_commands,zipf_param)
    sequence = generate_from_matrix(matrix,size)
    return sequence