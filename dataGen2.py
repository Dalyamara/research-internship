#Complexity 0
#Uniform Generate
import random
import numpy as np
import pandas as pd 





def get_goal_measure(num_commands=12,zipf_param=1) :
    '''
    Returns the stationary probability of a zipf distribution that we want to eventually achieve by a markov chain
    '''
    goal = np.zeros(num_commands)
    for i in range(num_commands):
        goal[i] = 1/ (i+1)**zipf_param
    goal = goal / goal.sum()
    return goal

def initialize_matrix(num_commands,sparsity) :
    '''
    initializes a transition matrix
    '''
    indexes = [(i,j) for i in range(num_commands) for j in range(num_commands)]
    P = np.zeros((num_commands,num_commands))
    irredcutible = False
     
    
        
    
    i = 0
    while not irredcutible :
        for i in range(num_commands) :
            P[i] = np.random.uniform(0.01,0.99,num_commands)
            P[i] = P[i] / P[i].sum()
        temp = P.copy()
        sparse_inds = random.sample(indexes,k=sparsity)
        for i,j in sparse_inds :
            P[i,j] = 0
        P = revert_to_one(P)
        irredcutible = verify_irreductibility(P)
        i += 1
        if i == 1000 :
            print("Can't do that much sparsity")
            return temp
    return P


def revert_to_one(matrix) :
    '''
    returns any matrix as a matrix in which every line sums up to 1
    '''
    for i in range(matrix.shape[0]) :
        matrix[i] = matrix[i]/matrix[i].sum()
    return matrix


def get_trial_pi(matrix) :
    '''
    Stationary probability of the matrix
    '''
    eigs = np.linalg.eig(matrix.T) 
    for index,eig_val in enumerate(eigs[0]) :
        if np.linalg.norm(eig_val - 1) < 0.001 :
            break
    trial_pi = eigs[1][:,index]
    trial_pi = trial_pi.real
    trial_pi = trial_pi / trial_pi.sum()
    return trial_pi




def verify_irreductibility(matrix) :
    '''
    Verifies if a matrix is irreductible
    '''
    dim = matrix.shape[0]
    carrier = matrix.copy()
    for i in range(dim):
        carrier += carrier @ matrix
    if 0 in carrier.reshape(-1) : 
        return False
    return True

#Zipf with Dependencies
def get_matrix(num_commands=12,zipf_param=1,sparsity=1) :
    '''
    gets a random transition matrix for which the stationary distribution follows a zipf
    '''

    
    goal = get_goal_measure(num_commands,zipf_param)
    P = initialize_matrix(num_commands,sparsity)
    conv = False
    max_iter = num_commands*1000
    eps = 0.02
    i=0
    exp = 0.5
    while not(conv) : 
        trial = get_trial_pi(P)
        # to_change = np.argmax(np.abs(trial-goal))
        to_change = np.random.randint(0,num_commands)

        choice = np.random.randint(0,num_commands)
        P[choice,to_change] = P[choice,to_change] * 1.5**( (goal-trial)[to_change] )
        P = revert_to_one(P)
        i+= 1
        conv = np.linalg.norm(trial-goal) < eps
        if i>max_iter : 
            # print("max iterations reached")
            return P
    return P



def transition_matrix(command_sequence) :
    '''
    gets a transition matrix from a sequence
    '''
    dim = len(set(command_sequence))
    Matrix = np.zeros((dim,dim))
    counts = {i: 0 for i in range(dim)}

    for e,actual in enumerate(command_sequence[1:]) :
        prev = command_sequence[e]
        Matrix[prev,actual] += 1
        counts[prev] += 1
    for i in range(dim) : 
        Matrix[i] = Matrix[i] / counts[i]
    return Matrix



def generate_from_matrix(matrix,size) :
    
    dim = matrix.shape[0]
    commands = [i for i in range(dim)]
    prev = np.random.choice(commands)
    sequence = [prev]
    for i in range(size-1) :
        actual = np.random.choice(commands,p = matrix[prev])
        prev = actual
        sequence.append(prev)
    return sequence

def zipfDep(size,num_commands=12,zipf_param=1,seed=None,sparsity=5) : 
    if seed is not None :
        np.random.seed(seed)
    matrix = get_matrix(num_commands,zipf_param,sparsity)
    sequence = generate_from_matrix(matrix,size)
    return sequence